#!/usr/bin/env bash

set -e

source ./versions.sh

SOURCE_DIR=dependencies/codeclimate

echo "Fetch codeclimate"
git clone -b ${CI_CODECLIMATE_VERSION} --depth 1 https://github.com/codeclimate/codeclimate.git ${SOURCE_DIR}

echo "Buld codeclimate"
gem build -C ${SOURCE_DIR} $PWD/${SOURCE_DIR}/codeclimate.gemspec

echo "Install codeclimate"
sudo gem install ${SOURCE_DIR}/codeclimate-*.gem
