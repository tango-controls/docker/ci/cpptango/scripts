#!/usr/bin/env bash

set -e

source ./versions.sh

SOURCE_DIR=dependencies/omniORB
BUILD_DIR=${SOURCE_DIR}/build
INSTALL_DIR=/home/tango

# restore CWD on script exit
STORED_CWD=${PWD}
trap "cd ${STORED_CWD}" EXIT

mkdir -p ${SOURCE_DIR}

echo "Fetch omniORB"
curl -L -o omniORB.tar.bz2 https://sourceforge.net/projects/omniorb/files/omniORB/omniORB-${CI_OMNIORB_VERSION_LATEST}/omniORB-${CI_OMNIORB_VERSION_LATEST}.tar.bz2/download
tar xaf omniORB.tar.bz2 --directory=${SOURCE_DIR} --strip-components=1

mkdir -p ${BUILD_DIR}
cd ${BUILD_DIR}

echo "Configure omniORB"
if [[ -n "${CROSSCOMPILE}" ]]
then
  if [[ "${CROSSCOMPILE}" == "i686" ]]
  then
     ../configure --host=i686-linux-gnu "CFLAGS=-m32" "CXXFLAGS=-m32" "LDFLAGS=-m32" --prefix=${INSTALL_DIR} --with-openssl
  else
     echo "Invalid value for CROSSCOMPILE=${CROSSCOMPILE}"
     exit 1
  fi
else
../configure --prefix=${INSTALL_DIR} --with-openssl
fi

echo "Build omniORB"
make -j$(nproc) all

echo "Install omniORB"
make install
