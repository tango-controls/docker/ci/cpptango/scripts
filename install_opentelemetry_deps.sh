#!/usr/bin/env bash

set -e

source ./versions.sh

source ./functions.sh

INSTALL_DIR=/home/tango

PACKAGE=abseil
SOURCE_DIR=dependencies/${PACKAGE}
BUILD_DIR=${SOURCE_DIR}/build

mkdir -p ${SOURCE_DIR}

echo "Fetch ${PACKAGE}"
curl -L -o ${PACKAGE}.tar.gz https://github.com/abseil/abseil-cpp/archive/refs/tags/${CI_ABSEIL_VERSION}.tar.gz
tar xaf ${PACKAGE}.tar.gz --directory=${SOURCE_DIR} --strip-components=1

echo "Configure ${PACKAGE}"
cmake -S ${SOURCE_DIR} -B ${BUILD_DIR}                                                            \
      -DBUILD_TESTING=OFF -DCMAKE_BUILD_TYPE=RelWithDebInfo -DCMAKE_INSTALL_PREFIX=${INSTALL_DIR} \
      -DABSL_ENABLE_INSTALL=ON -DCMAKE_POSITION_INDEPENDENT_CODE=ON -DBUILD_SHARED_LIBS=ON        \
      -DABSL_PROPAGATE_CXX_STD=ON -DCMAKE_CXX_STANDARD=${CI_CXX_STANDARD}

echo "Build ${PACKAGE}"
cmake --build ${BUILD_DIR}

echo "Install ${PACKAGE}"
cmake --build ${BUILD_DIR} --target install

ldconfig_wrapper

PACKAGE=protobuf
SOURCE_DIR=dependencies/${PACKAGE}
BUILD_DIR=${SOURCE_DIR}/build

mkdir -p ${SOURCE_DIR}

curl -L -o ${PACKAGE}.tar.gz https://github.com/protocolbuffers/protobuf/archive/refs/tags/${CI_PROTOBUF_VERSION}.tar.gz
tar xaf ${PACKAGE}.tar.gz --directory=${SOURCE_DIR} --strip-components=1

cmake -S ${SOURCE_DIR} -B ${BUILD_DIR}                                                             \
      -Dprotobuf_BUILD_TESTS=OFF -DCMAKE_BUILD_TYPE=RelWithDebInfo                                 \
      -DCMAKE_PREFIX_PATH=${INSTALL_DIR} -DCMAKE_INSTALL_PREFIX=${INSTALL_DIR}                     \
      -DCMAKE_POSITION_INDEPENDENT_CODE=ON -DBUILD_SHARED_LIBS=ON -Dprotobuf_ABSL_PROVIDER=package \
      -Dprotobuf_BUILD_EXAMPLES=OFF                                                                \
      -DCMAKE_CXX_STANDARD=${CI_CXX_STANDARD}

echo "Build ${PACKAGE}"
cmake --build ${BUILD_DIR}

echo "Install ${PACKAGE}"
cmake --build ${BUILD_DIR} --target install

ldconfig_wrapper

PACKAGE=gRPC
SOURCE_DIR=dependencies/${PACKAGE}
BUILD_DIR=${SOURCE_DIR}/build

mkdir -p ${SOURCE_DIR}

curl -L -o ${PACKAGE}.tar.gz https://github.com/grpc/grpc/archive/refs/tags/${CI_GRPC_VERSION}.tar.gz
tar xaf ${PACKAGE}.tar.gz --directory=${SOURCE_DIR} --strip-components=1

patch -d ${SOURCE_DIR} -p1 <${CI_SCRIPT_DIR}/patches/0001-Fix-new-clang-Wmissing-template-arg-list-after-templ.patch

cmake -S ${SOURCE_DIR} -B ${BUILD_DIR}                                         \
      -DgRPC_BUILD_TESTS=OFF -DCMAKE_BUILD_TYPE=RelWithDebInfo                 \
      -DCMAKE_PREFIX_PATH=${INSTALL_DIR} -DCMAKE_INSTALL_PREFIX=${INSTALL_DIR} \
      -DCMAKE_POSITION_INDEPENDENT_CODE=ON -DBUILD_SHARED_LIBS=ON              \
      -DgRPC_ABSL_PROVIDER=package -DgRPC_CARES_PROVIDER=package               \
      -DgRPC_PROTOBUF_PROVIDER=package -DgRPC_RE2_PROVIDER=package             \
      -DgRPC_SSL_PROVIDER=package -DgRPC_ZLIB_PROVIDER=package                 \
      -DgRPC_INSTALL=ON                                                        \
      -DCMAKE_CXX_STANDARD=${CI_CXX_STANDARD}

echo "Build ${PACKAGE}"
cmake --build ${BUILD_DIR}

echo "Install ${PACKAGE}"
cmake --build ${BUILD_DIR} --target install

ldconfig_wrapper
