#!/usr/bin/env bash

set -e

function ldconfig_wrapper
{
  # try first the glibc variant and then the busybox one
  sudo ldconfig || sudo ldconfig /
}

# SOURCE_DIR must to point to a git repository
# PACKAGE_NAME be the name of the package
function gather_version_info
{
  local COMMIT_HASH=$(git --git-dir="${SOURCE_DIR}/.git" log -n1 --pretty=%H)

  echo "Using revision ${COMMIT_HASH} of package ${PACKAGE_NAME}"

  mkdir -p ~/versions
  echo ${COMMIT_HASH} > ~/versions/${PACKAGE_NAME}.txt
}
