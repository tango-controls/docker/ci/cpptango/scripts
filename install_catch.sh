#!/usr/bin/env bash

set -e

source ./versions.sh

SOURCE_DIR=dependencies/catch
BUILD_DIR=${SOURCE_DIR}/build
INSTALL_DIR=/home/tango

function config_build_install_catch() {
  echo "Configure catch ($BUILD_DIR)"
  cmake                                   \
    -H${SOURCE_DIR}                       \
    -B${BUILD_DIR}                        \
    -DCMAKE_POSITION_INDEPENDENT_CODE=ON  \
    -DCMAKE_INSTALL_PREFIX=${INSTALL_DIR} \
    -DCATCH_INSTALL_DOCS=OFF              \
    -DCATCH_BUILD_TESTING=OFF             \
    -DCATCH_ENABLE_WERROR=OFF             \
    ${CMAKE_EXTRA_ARGS}

  echo "Build catch ($BUILD_DIR)"
  make -C ${BUILD_DIR}

  echo "Install catch ($BUILD_DIR)"
  make -C ${BUILD_DIR} install
}

if [[ -n "${CROSSCOMPILE}" ]]; then
  if [[ "${CXX}" =~ clang++ ]]; then
    echo "CROSSCOMPILE not supported with CXX=${CXX}"
    exit 1
  fi
  if [[ "${CROSSCOMPILE}" == "i686" ]]; then
    CMAKE_EXTRA_ARGS="-DCMAKE_SYSTEM_NAME=Linux \
      -DCMAKE_SYSTEM_PROCESSOR=i686 \
      -DCMAKE_CROSSCOMPILING=TRUE \
      -DCMAKE_CXX_FLAGS_INIT=-m32 \
      -DCMAKE_EXE_LINKER_FLAGS_INIT=-m32 \
      -DCMAKE_SHARED_LINKER_FLAGS_INIT=-m32 \
      -DCMAKE_MODULE_LINKER_FLAGS_INIT=-m32"
  else
     echo "Invalid value for CROSSCOMPILE=${CROSSCOMPILE}"
     exit 1
  fi
fi

echo "Fetch catch"
git clone -b ${CI_CATCH_VERSION} --depth 1 https://github.com/catchorg/Catch2.git ${SOURCE_DIR}

config_build_install_catch

if [[ "${CXX}" =~ clang++ ]]; then
  BUILD_DIR=${SOURCE_DIR}/build-libcpp
  INSTALL_DIR=/home/tango/catch-libcpp
  CMAKE_EXTRA_ARGS="-DCMAKE_CXX_FLAGS_INIT=-stdlib=libc++ \
    -DCMAKE_EXE_LINKER_FLAGS_INIT=-stdlib=libc++ \
    -DCMAKE_SHARED_LINKER_FLAGS_INIT=-stdlib=libc++ \
    -DCMAKE_MODULE_LINKER_FLAGS_INIT=-stdlib=libc++"
  config_build_install_catch
fi

