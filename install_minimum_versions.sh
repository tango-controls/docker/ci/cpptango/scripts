#!/usr/bin/env bash

set -e

source ./versions.sh

source ./functions.sh

# This script installs the minimum required version of every build dependency of cppTango.

SOURCE_PREFIX=/tmp
INSTALL_PREFIX=/tmp/install

# Install cmake
PACKAGE=cmake
SOURCE_DIR=${SOURCE_PREFIX}/${PACKAGE}
INSTALL_DIR=${INSTALL_PREFIX}/${PACKAGE}
mkdir -p ${SOURCE_DIR}
CI_CMAKE_VERSION_SHORT=$(echo ${CI_CMAKE_VERSION} | cut -d '.' -f 1-2)
curl -L https://cmake.org/files/v${CI_CMAKE_VERSION_SHORT}/cmake-${CI_CMAKE_VERSION}-Linux-x86_64.sh -o ${SOURCE_DIR}/cmake-install.sh
chmod +x ${SOURCE_DIR}/cmake-install.sh
mkdir -p ${INSTALL_DIR}
cd ${INSTALL_DIR}
${SOURCE_DIR}/cmake-install.sh --skip-license --exclude-subdir
export PATH=${PATH}:${INSTALL_DIR}/bin

# Install tango-idl
PACKAGE=tango-idl
SOURCE_DIR=${SOURCE_PREFIX}/${PACKAGE}
BUILD_DIR=${SOURCE_DIR}/build
INSTALL_DIR=${INSTALL_PREFIX}/${PACKAGE}
git clone -b ${CI_TANGO_IDL_VERSION} --depth 1 https://gitlab.com/tango-controls/tango-idl.git ${SOURCE_DIR}
cmake -S ${SOURCE_DIR} -B ${BUILD_DIR} -DCMAKE_INSTALL_PREFIX=${INSTALL_DIR}
cmake --build ${BUILD_DIR} --target install

# Install libzmq
PACKAGE=libzmq
SOURCE_DIR=${SOURCE_PREFIX}/${PACKAGE}
BUILD_DIR=${SOURCE_DIR}/build
INSTALL_DIR=${INSTALL_PREFIX}/${PACKAGE}
git clone -b ${CI_LIBZMQ_VERSION} --depth 1 https://github.com/zeromq/zeromq4-x.git ${SOURCE_DIR}
cmake -S ${SOURCE_DIR} -B ${BUILD_DIR} -DCMAKE_INSTALL_PREFIX=${INSTALL_DIR}
cmake --build ${BUILD_DIR} --target install

# Install cppzmq
PACKAGE=cppzmq
SOURCE_DIR=${SOURCE_PREFIX}/${PACKAGE}
BUILD_DIR=${SOURCE_DIR}/build
INSTALL_DIR=${INSTALL_PREFIX}/${PACKAGE}
git clone -b ${CI_CPPZMQ_VERSION} --depth 1 https://github.com/zeromq/cppzmq.git ${SOURCE_DIR}
PKG_CONFIG_PATH=${INSTALL_PREFIX}/libzmq/lib/pkgconfig cmake \
  -S ${SOURCE_DIR}                                           \
  -B ${BUILD_DIR}                                            \
  -DCPPZMQ_BUILD_TESTS=OFF                                   \
  -DCMAKE_INSTALL_PREFIX=${INSTALL_DIR}
  cmake --build ${BUILD_DIR} --target install

# Install catch
NAME=catch
SOURCE_DIR=${SOURCE_PREFIX}/${NAME}
BUILD_DIR=${SOURCE_DIR}/build
INSTALL_DIR=${INSTALL_PREFIX}/${NAME}
git clone -b ${CI_CATCH_VERSION} --depth 1 https://github.com/catchorg/Catch2.git ${SOURCE_DIR}
cmake -S ${SOURCE_DIR}                      \
      -B ${BUILD_DIR}                       \
      -DCMAKE_POSITION_INDEPENDENT_CODE=ON  \
      -DCMAKE_INSTALL_PREFIX=${INSTALL_DIR} \
      -DCATCH_INSTALL_DOCS=OFF              \
      -DCATCH_BUILD_TESTING=OFF             \
      -DCATCH_ENABLE_WERROR=OFF
cmake --build ${BUILD_DIR}
cmake --build ${BUILD_DIR} --target install

# Install omniORB
PACKAGE=omniORB
SOURCE_DIR=${SOURCE_PREFIX}/${PACKAGE}
BUILD_DIR=${SOURCE_DIR}/build
INSTALL_DIR=${INSTALL_PREFIX}/${PACKAGE}
mkdir -p ${SOURCE_DIR}
curl -L https://sourceforge.net/projects/omniorb/files/omniORB/omniORB-${CI_OMNIORB_VERSION}/omniORB-${CI_OMNIORB_VERSION}.tar.bz2/download -o ${SOURCE_DIR}/omniORB.tar.bz2
tar xaf ${SOURCE_DIR}/omniORB.tar.bz2 --strip-components=1 -C ${SOURCE_DIR}
mkdir -p ${BUILD_DIR}
cd ${BUILD_DIR}
${SOURCE_DIR}/configure --prefix=${INSTALL_DIR}
make -C${BUILD_DIR}
make -C${BUILD_DIR} install

# Install libjpeg
PACKAGE=libjpeg
SOURCE_DIR=${SOURCE_PREFIX}/${PACKAGE}
BUILD_DIR=${SOURCE_DIR}/build
INSTALL_DIR=${INSTALL_PREFIX}/${PACKAGE}
git clone -b ${CI_LIBJPEG_VERSION} --depth 1 https://github.com/libjpeg-turbo/libjpeg-turbo.git ${SOURCE_DIR}
cd ${SOURCE_DIR}
autoreconf -fiv
mkdir -p ${BUILD_DIR}
cd ${BUILD_DIR}
${SOURCE_DIR}/configure -- prefix=${INSTALL_DIR}
make -C${BUILD_DIR}
make -C${BUILD_DIR} install

# Install abseil
PACKAGE=abseil
SOURCE_DIR=${SOURCE_PREFIX}/${PACKAGE}
BUILD_DIR=${SOURCE_DIR}/build
INSTALL_DIR=${INSTALL_PREFIX}/${PACKAGE}
mkdir -p ${SOURCE_DIR}
curl -L -o ${PACKAGE}.tar.gz https://github.com/abseil/abseil-cpp/archive/refs/tags/${CI_ABSEIL_VERSION}.tar.gz
tar xaf ${PACKAGE}.tar.gz --directory=${SOURCE_DIR} --strip-components=1
cmake -S ${SOURCE_DIR} -B ${BUILD_DIR}                                                            \
      -DBUILD_TESTING=OFF -DCMAKE_BUILD_TYPE=RelWithDebInfo -DCMAKE_INSTALL_PREFIX=${INSTALL_DIR} \
      -DABSL_ENABLE_INSTALL=ON -DCMAKE_POSITION_INDEPENDENT_CODE=ON -DBUILD_SHARED_LIBS=ON        \
      -DABSL_PROPAGATE_CXX_STD=ON -DCMAKE_CXX_STANDARD=${CI_CXX_STANDARD}
cmake --build ${BUILD_DIR} --target install

echo "/tmp/install/${PACKAGE}/lib" | sudo tee -a /etc/ld.so.conf.d/${PACKAGE}.conf
ldconfig_wrapper

# Install protobuf
PACKAGE=protobuf
SOURCE_DIR=${SOURCE_PREFIX}/${PACKAGE}
BUILD_DIR=${SOURCE_DIR}/build
INSTALL_DIR=${INSTALL_PREFIX}/${PACKAGE}
mkdir -p ${SOURCE_DIR}
curl -L -o ${PACKAGE}.tar.gz https://github.com/protocolbuffers/protobuf/archive/refs/tags/${CI_PROTOBUF_VERSION}.tar.gz
tar xaf ${PACKAGE}.tar.gz --directory=${SOURCE_DIR} --strip-components=1
cmake -S ${SOURCE_DIR} -B ${BUILD_DIR}                                                             \
      -Dprotobuf_BUILD_TESTS=OFF -DCMAKE_BUILD_TYPE=RelWithDebInfo                                 \
      -DCMAKE_PREFIX_PATH=${INSTALL_DIR} -DCMAKE_INSTALL_PREFIX=${INSTALL_DIR}                     \
      -DCMAKE_POSITION_INDEPENDENT_CODE=ON -DBUILD_SHARED_LIBS=ON -Dprotobuf_ABSL_PROVIDER=package \
      -Dprotobuf_BUILD_EXAMPLES=OFF                                                                \
      -DCMAKE_CXX_STANDARD=${CI_CXX_STANDARD}                                                      \
      -DCMAKE_PREFIX_PATH="/tmp/install/abseil/lib/cmake"
cmake --build ${BUILD_DIR} --target install
echo "/tmp/install/${PACKAGE}/lib" | sudo tee -a /etc/ld.so.conf.d/${PACKAGE}.conf
ldconfig_wrapper

# Install gRPC
PACKAGE=gRPC
SOURCE_DIR=${SOURCE_PREFIX}/${PACKAGE}
BUILD_DIR=${SOURCE_DIR}/build
INSTALL_DIR=${INSTALL_PREFIX}/${PACKAGE}
mkdir -p ${SOURCE_DIR}
curl -L -o ${PACKAGE}.tar.gz https://github.com/grpc/grpc/archive/refs/tags/${CI_GRPC_VERSION}.tar.gz
tar xaf ${PACKAGE}.tar.gz --directory=${SOURCE_DIR} --strip-components=1
cmake -S ${SOURCE_DIR} -B ${BUILD_DIR}                                                     \
      -DgRPC_BUILD_TESTS=OFF -DCMAKE_BUILD_TYPE=RelWithDebInfo                             \
      -DCMAKE_PREFIX_PATH=${INSTALL_DIR} -DCMAKE_INSTALL_PREFIX=${INSTALL_DIR}             \
      -DCMAKE_POSITION_INDEPENDENT_CODE=ON -DBUILD_SHARED_LIBS=ON                          \
      -DgRPC_ABSL_PROVIDER=package -DgRPC_CARES_PROVIDER=package                           \
      -DgRPC_PROTOBUF_PROVIDER=package -DgRPC_RE2_PROVIDER=package                         \
      -DgRPC_SSL_PROVIDER=package -DgRPC_ZLIB_PROVIDER=package                             \
      -DgRPC_INSTALL=ON                                                                    \
      -DCMAKE_CXX_STANDARD=${CI_CXX_STANDARD}                                              \
      -DCMAKE_PREFIX_PATH="/tmp/install/abseil/lib/cmake;/tmp/install/protobuf/lib/cmake"
cmake --build ${BUILD_DIR} --target install
echo "/tmp/install/${PACKAGE}/lib" | sudo tee -a /etc/ld.so.conf.d/${PACKAGE}.conf
ldconfig_wrapper

# Install opentelemetry
PACKAGE=opentelemetry
SOURCE_DIR=${SOURCE_PREFIX}/${PACKAGE}
BUILD_DIR=${SOURCE_DIR}/build
INSTALL_DIR=${INSTALL_PREFIX}/${PACKAGE}
mkdir -p ${SOURCE_DIR}
curl -L -o ${PACKAGE}.tar.gz https://github.com/open-telemetry/opentelemetry-cpp/archive/refs/tags/${CI_OPENTELEMETRY_VERSION_EARLIEST}.tar.gz
tar xaf ${PACKAGE}.tar.gz --directory=${SOURCE_DIR} --strip-components=1
cmake -S ${SOURCE_DIR} -B ${BUILD_DIR}                                                                                 \
      -DBUILD_TESTING=OFF -DCMAKE_BUILD_TYPE=RelWithDebInfo                                                            \
      -DCMAKE_PREFIX_PATH=${INSTALL_DIR} -DCMAKE_INSTALL_PREFIX=${INSTALL_DIR}                                         \
      -DCMAKE_POSITION_INDEPENDENT_CODE=ON -DBUILD_SHARED_LIBS=ON                                                      \
      -DWITH_OTLP_GRPC=ON -DWITH_OTLP_HTTP=ON -DWITH_ABSEIL=ON -DWITH_BENCHMARK=OFF                                    \
      -DWITH_EXAMPLES=OFF -DWITH_FUNC_TESTS=OFF -DCMAKE_CXX_STANDARD=${CI_CXX_STANDARD}                                \
      -DCMAKE_PREFIX_PATH="/tmp/install/abseil/lib/cmake;/tmp/install/protobuf/lib/cmake;/tmp/install/gRPC/lib/cmake"
cmake --build ${BUILD_DIR} --target install
echo "/tmp/install/${PACKAGE}/lib" | sudo tee -a /etc/ld.so.conf.d/${PACKAGE}.conf
ldconfig_wrapper
