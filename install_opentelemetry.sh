#!/usr/bin/env bash

set -e

source ./versions.sh

source ./functions.sh

INSTALL_DIR=/home/tango

PACKAGE=opentelemetry
SOURCE_DIR=dependencies/${PACKAGE}
BUILD_DIR=${SOURCE_DIR}/build

mkdir -p ${SOURCE_DIR}

curl -L -o ${PACKAGE}.tar.gz https://github.com/open-telemetry/opentelemetry-cpp/archive/refs/tags/${CI_OPENTELEMETRY_VERSION}.tar.gz
tar xaf ${PACKAGE}.tar.gz --directory=${SOURCE_DIR} --strip-components=1

patch -d ${SOURCE_DIR} -p1 <${CI_SCRIPT_DIR}/patches/otel-fix-llvm-19-compilation.patch

cmake -S ${SOURCE_DIR} -B ${BUILD_DIR}                                                  \
      -DBUILD_TESTING=OFF -DCMAKE_BUILD_TYPE=RelWithDebInfo                             \
      -DCMAKE_PREFIX_PATH=${INSTALL_DIR} -DCMAKE_INSTALL_PREFIX=${INSTALL_DIR}          \
      -DCMAKE_POSITION_INDEPENDENT_CODE=ON -DBUILD_SHARED_LIBS=ON                       \
      -DWITH_OTLP_GRPC=ON -DWITH_OTLP_HTTP=ON -DWITH_ABSEIL=ON -DWITH_BENCHMARK=OFF     \
      -DWITH_EXAMPLES=OFF -DWITH_FUNC_TESTS=OFF -DCMAKE_CXX_STANDARD=${CI_CXX_STANDARD} \
      -DWITH_DEPRECATED_SDK_FACTORY=OFF

echo "Build ${PACKAGE}"
cmake --build ${BUILD_DIR}

echo "Install ${PACKAGE}"
cmake --build ${BUILD_DIR} --target install

ldconfig_wrapper
