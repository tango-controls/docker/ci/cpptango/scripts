#!/usr/bin/env bash

set -e

source ./versions.sh
source ./functions.sh

PACKAGE_NAME=cppTango
SOURCE_DIR=dependencies/${PACKAGE_NAME}
BUILD_DIR=${SOURCE_DIR}/build
INSTALL_DIR=/home/tango

echo "Fetch ${PACKAGE_NAME}"
git clone --recurse-submodules -b ${CI_CPPTANGO_VERSION} --depth 1 https://gitlab.com/tango-controls/cppTango.git ${SOURCE_DIR}

gather_version_info

echo "Configure ${PACKAGE_NAME}"
cmake -H${SOURCE_DIR} -B${BUILD_DIR} -DCMAKE_INSTALL_PREFIX=${INSTALL_DIR}            \
      -DCMAKE_POSITION_INDEPENDENT_CODE=ON -DBUILD_SHARED_LIBS=ON -DBUILD_TESTING=OFF \
      -DTANGO_USE_TELEMETRY=ON -DCMAKE_BUILD_TYPE=RelWithDebInfo -GNinja              \
      -DCMAKE_CXX_STANDARD=17

echo "Build and install ${PACKAGE_NAME}"
cmake --build ${BUILD_DIR} --target install
