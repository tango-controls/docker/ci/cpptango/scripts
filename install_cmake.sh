#!/usr/bin/env bash

# required environment variables:
# CI_CMAKE_VERSION

SOURCE_PREFIX=/tmp
INSTALL_PREFIX=/tmp/install

set -e

NAME=cmake
SOURCE_DIR=${SOURCE_PREFIX}/${NAME}
INSTALL_DIR=${INSTALL_PREFIX}/${NAME}
mkdir -p ${SOURCE_DIR}
CI_CMAKE_VERSION_SHORT=$(echo ${CI_CMAKE_VERSION} | cut -d '.' -f 1-2)
curl -L https://cmake.org/files/v${CI_CMAKE_VERSION_SHORT}/cmake-${CI_CMAKE_VERSION}-linux-x86_64.sh -o ${SOURCE_DIR}/cmake-install.sh
chmod +x ${SOURCE_DIR}/cmake-install.sh
mkdir -p ${INSTALL_DIR}
cd ${INSTALL_DIR}
${SOURCE_DIR}/cmake-install.sh --skip-license --exclude-subdir
