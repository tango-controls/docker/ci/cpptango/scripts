#!/usr/bin/env bash

set -e

source ./versions.sh

SOURCE_DIR=dependencies/cppzmq
BUILD_DIR=${SOURCE_DIR}/build
INSTALL_DIR=/home/tango

echo "Fetch cppzmq"
git clone -b ${CI_CPPZMQ_VERSION} --depth 1 https://github.com/zeromq/cppzmq.git ${SOURCE_DIR}

echo "Build cppzmq"
cmake -H${SOURCE_DIR} -B${BUILD_DIR} -DCMAKE_INSTALL_PREFIX=${INSTALL_DIR} -DCPPZMQ_BUILD_TESTS=OFF

echo "Install cppzmq"
make -C ${BUILD_DIR} install
