#!/usr/bin/env bash

set -e

source ./versions.sh

SOURCE_DIR=dependencies/idl
BUILD_DIR=${SOURCE_DIR}/build
INSTALL_DIR=/home/tango

echo "Fetch tango-idl"
git clone -b ${CI_TANGO_IDL_VERSION} --depth 1 https://gitlab.com/tango-controls/tango-idl.git ${SOURCE_DIR}

echo "Build tango-idl"
cmake -H${SOURCE_DIR} -B${BUILD_DIR} -DCMAKE_INSTALL_PREFIX=${INSTALL_DIR}

echo "Install tango-idl"
make -C ${BUILD_DIR} install
