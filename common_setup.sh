#!/usr/bin/env bash

set -e

source ./versions.sh

# avoid git warnings in CI, see https://gitlab.com/tango-controls/cppTango/-/issues/1189
git config --global --add safe.directory '*'

git config --global advice.detachedHead false
